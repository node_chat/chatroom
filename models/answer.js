var mongoose = require("mongoose");
var bcrypt = require('bcrypt-nodejs');
var mongo = require('mongodb');
var config = require('../config');
var async = require("async");
var ObjectID = mongo.ObjectID;

var AnswerModel = mongoose.model("Answer", function(){

	var s = new mongoose.Schema({
		_id:{
			type: String,
			required: true,
		},
		question_id:{
			type: String,
			required: true
		},
                answer:{
			type: String,
			required: true
		}
	},
	{
        timestamps: true
    });
    
    s.statics.saveAnswer = function(data, callback){
        new AnswerModel(data).save(function (err, response_data) {
            if (!err) {
                callback({ "response_code":2000,"result":response_data});
            } else {
                  callback({
                    "response_code":5005,
                    "response_message":"INTERNAL DB ERROR",
                    "response_data":{}
                })
            }
        });
    }

    return s;
}());

module.exports = AnswerModel;