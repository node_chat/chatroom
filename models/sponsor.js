var mongoose = require("mongoose");
var bcrypt = require('bcrypt-nodejs');
var mongo = require('mongodb');
var config = require('../config');
var async = require("async");
var ObjectID = mongo.ObjectID;

var SponsorModel = mongoose.model("Sponsor", function(){

	var s = new mongoose.Schema({
		_id:{
			type: String,
			required: true,
		},
		sponsor_url:{
			type: String,
			required: true
		},
                sponsor_image:{
			type: String,
			required: true
		}
	},
	{
        timestamps: true
    });
    
    s.statics.saveSponsor = function(data, callback){
        new SponsorModel(data).save(function (err, response_data) {
            if (!err) {
                callback({ "response_code":2000,"result":response_data});
            } else {
                  callback({
                    "response_code":5005,
                    "response_message":"INTERNAL DB ERROR",
                    "response_data":{}
                })
            }
        });
    }
    
    s.statics.getList = function(data, callback){
        SponsorModel.find({
            
        },{updatedAt:0,createdAt:0,__v:0},function(err,r){
            if(err) console.log(err);
            else{ //console.log(r); return;
                if(r.length > 0){
                    callback({
                        "response_code": 2000,
                        "response_data": r
                    })
                }else{
                    callback({
                        "response_code": 3000,
                        "response_data": {}
                    })
                }
            }
        })
    }
    
    s.statics.spotLightSponsor = function(data, callback){
        SponsorModel.aggregate([
                {
                    $match: {
                        
                    }
                }, 
                { $limit : 4 },
                {
                    $sort: {updatedAt: -1}
                }
            ], function (err, res) { //console.log(res); return;
                if (err) {
                    callback({
                        "response_code": 5005,
                        "response_message": "INTERNAL DB ERROR",
                        "response_data": err
                    })
                }
                if (!err) {
                    if(res.length > 0){
                        
                        async.forEach(res, function (item, callback) {
                            item.sponsor_image = item.sponsor_image ? config.liveUrl + config.sponsorImagePath + item.sponsor_image : '';
                            callback();
                        }, function (err) {
                            if (err) {
                                    console.log(err);
                            }
                            if(!err){

                            callback({ "response_code":2000,"result":res});
                            }
                        }); 
                    }else{
                        callback({ "response_code": 2001, "result": []});
                    }
                }
            })
    }

    return s;
}());

module.exports = SponsorModel;