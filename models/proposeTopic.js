var mongoose = require("mongoose");
var bcrypt = require('bcrypt-nodejs');
var config = require('../config');
var async = require("async");

var ProposeTopicModels = mongoose.model("ProposeTopic", function () {
    var s = new mongoose.Schema({
        _id: {
            type: String,
            required: true,
        },
        user_id: {
            type: String,
            required: true
        },
        user_type: {
            type: String,
            default: ''
        },
        topic_name: {
            type: String,
            default: ''
        },
        topic_desc: {
            type: String,
            default: ''
        },
        main_cat_id: {
            type: String,
            default: ''
        },
        main_cat_name: {
            type: String,
            default: ''
        },
        accepted_list: {
            type: Array,
            default: []
        },
        creation_time: {
            type: Date,
            default: ''
        }
    },
        {
            timestamps: true
        });
    s.statics.createEditProposeTopic = function (proposeTopicData, callback) {
        if (proposeTopicData.topic_id) {
            //update the topic 
            ProposeTopicModels.update({
                _id: proposeTopicData.topic_id,
                user_id:proposeTopicData.user_id
            }, {
                    $set: {
                        topic_desc: proposeTopicData.topic_desc,
                        main_cat_id: proposeTopicData.main_cat_id,
                        topic_name: proposeTopicData.topic_name,
                        main_cat_name: proposeTopicData.main_cat_name
                    }
                }).exec(function (err, u) {
                    if (err) {
                        callback({
                            "response_code": 5005,
                            "response_message": "INTERNAL DB ERROR",
                            "response_data": { err }
                        })
                    }
                    if (!err) {
                        if (u.n === 1 && u.nModified === 1) {
                            callback({
                                "response_code": 2000,
                                "response_message": "Success.",
                                "response_data": {}
                            })
                        }
                        else {
                            callback({
                                "response_code": 5005,
                                "response_message": "INTERNAL DB ERROR",
                                "response_data": { u }
                            })
                        }
                    }
                })
        }
        if(!proposeTopicData.topic_id) {
            //create new topic
            new ProposeTopicModels(proposeTopicData).save(function (err, pt) {
                if (err) {
                    callback({
                        "response_code": 5005,
                        "response_message": "INTERNAL DB ERROR",
                        "response_data": { err }
                    })
                }
                if (!err) {
                    if (pt) {
                        callback({
                            "response_code": 2000,
                            "response_message": "Success.",
                            "response_data": {
                                "topic_proposed": {
                                    "cat_id": pt.main_cat_id,
                                    "topic_id": pt._id,
                                    "topic_name": pt.topic_name,
                                    "topic_desc": pt.topic_desc,
                                    "topic_date": pt.createdAt,
                                }
                            }
                        })
                    }
                }
            })
        }

    }
    s.statics.proposedTopicList = function (proposedListData, callback) {
        if (proposedListData.user_type === 'Learner') {
            
            
            ProposeTopicModels.aggregate([
                {
                    $match: {
                        "user_id": proposedListData.user_id
                    }
                }, {
                    $sort: {updatedAt: -1}
                },
                {
                    $lookup: {
                        from: "users",
                        localField: "user_id",
                        foreignField: "_id",
                        as: "user_details"
                    }
                },
                {
                    $unwind: "$user_details"
                }, {
                    $project: {
                        "_id":1,
                        "user_id": 1,
                        "main_cat_id": 1,
                        "topic_desc": 1,
                        "topic_name": 1,
                        "creation_time":1,
                        "main_cat_name":1,
                        "fname": "$user_details.fname",
                        "lname": "$user_details.lname"
                    }
                }
            ], function (err, res) { //console.log(res); return;
                if (err) {
                    callback({
                        "response_code": 5005,
                        "response_message": "INTERNAL DB ERROR",
                        "response_data": err
                    })
                }
                if (!err) {
                    async.forEach(res, function (item, callback) {
                        item.creator_name = item.fname + " " + item.lname;
                        item.creation_time = new Date(item.creation_time).getTime();
                        callback();
                    }, function (err) {
                        if (err) {
                            callback({
                                "response_code": 5005,
                                "response_message": "INTERNAL DB ERROR",
                                "response_data": { err }
                            }) 
                        }
                        if(!err){
                                
                        callback({
                            "response_code": 2000,
                            "response_message": "Sucessfully saved.",
                            "response_data": {
                                "promotion_list":res
                            }
                        });
                        }
                    });
                }
            })
            
            
            
            /*ProposeTopicModels.find({
                user_id: proposedListData.user_id,
            },{__v:0,updatedAt:0},function(err,topicList){
                if(err){
                    callback({
                        "response_code": 5005,
                        "response_message": "INTERNAL DB ERROR",
                        "response_data": { err }
                    }) 
                }
                if(!err){
                    callback({
                        "response_code": 2000,
                        "response_message": "Sucessfully saved.",
                        "response_data": {
                            "promotion_list":topicList
                        }
                    });
                }
            })*/
        }
        if (proposedListData.user_type === 'Tutor') {
            /*ProposeTopicModels.find({},{__v:0,updatedAt:0},function(err,topicList){
                if(err){
                    callback({
                        "response_code": 5005,
                        "response_message": "INTERNAL DB ERROR",
                        "response_data": { err }
                    }) 
                }
                if(!err){
                    callback({
                        "response_code": 2000,
                        "response_message": "Sucessfully saved.",
                        "response_data": {
                            "promotion_list":topicList
                        }
                    });
                }
            })*/
            
            ProposeTopicModels.aggregate([
                {
                    $match: {
                        
                    }
                }, {
                    $sort: {updatedAt: -1}
                },
                {
                    $lookup: {
                        from: "users",
                        localField: "user_id",
                        foreignField: "_id",
                        as: "user_details"
                    }
                },
                {
                    $unwind: "$user_details"
                }, {
                    $project: {
                        "_id":1,
                        "user_id": 1,
                        "main_cat_id": 1,
                        "topic_desc": 1,
                        "topic_name": 1,
                        "creation_time":1,
                        "main_cat_name":1,
                        "fname": "$user_details.fname",
                        "lname": "$user_details.lname"
                    }
                }
            ], function (err, res) { //console.log(res); return;
                if (err) {
                    callback({
                        "response_code": 5005,
                        "response_message": "INTERNAL DB ERROR",
                        "response_data": err
                    })
                }
                if (!err) {
                    async.forEach(res, function (item, callback) {
                        item.creator_name = item.fname + " " + item.lname;
                        item.creation_time = new Date(item.creation_time).getTime();
                        callback();
                    }, function (err) {
                        if (err) {
                            callback({
                                "response_code": 5005,
                                "response_message": "INTERNAL DB ERROR",
                                "response_data": { err }
                            }) 
                        }
                        if(!err){
                                
                        callback({
                            "response_code": 2000,
                            "response_message": "Sucessfully saved.",
                            "response_data": {
                                "promotion_list":res
                            }
                        });
                        }
                    });
                }
            })
        }

    }
    s.statics.getLatestProposedTopic = function () {
        ProposeTopicModels.aggregate([{
            $match:{
               main_cat_id:{$in:{}} 
            }
        },{
            $lookup: {
                from: "users",
                localField: "user_id",
                foreignField: "_id",
                as: "userdata"
            }
        }, {
            $unwind:"$userdata"
        },{
           $project:{

           } 
        }])
    }
    return s;
}());

module.exports = ProposeTopicModels;