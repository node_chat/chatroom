var mongoose = require("mongoose");
var bcrypt = require('bcrypt-nodejs');
var config = require('../config');
var async = require("async");

var TopicModels = mongoose.model("Topics", function () {
    var s = new mongoose.Schema({
        _id: {
            type: String,
            required: true,
        },
        user_id: {
            type: String,
            required: true
        },
        main_cat_id: {
            type: String,
            default: '',
            required: true
        },
        main_cat_name: {
            type: String,
            default: '',
        },
        sub_cat_id: {
            type: String,
            default: ''
        },
        sub_cat_name: {
            type: String,
            default: ''
        },
        topic_header: {
            type: String,
            default: '',
            required: true
        },
        topic_description: {
            type: String,
            default: ''
        },
        learn_description: {
            type: String,
            default: ''
        },
        price: {
            type: Number,
            default: ''
        },
        session_duration: {
            type: Number,
            default: 0
        },
        image_url: {
            type: String,
            default: ''
        },
        topic_mode: {
            type: String,
            enum: ['ALL', 'ACTIVE', 'DISABLE', 'UNDERREVIEW', 'PROMOTED'],
            default: 'ACTIVE',
            required: true
        },
        promoted: {
            type: Boolean,
            default: false
        },
        views: {
            type: Number,
            default: 0
        },
        bookings: {
            type: Number,
            default: 0
        },
        rating: {
            type: Number,
            default: 0
        },
        start_date: {
            type: Date,
            default: 0
        },
        start_date_epoch: {
            type: Number,
            default: 0
        },
        end_date: {
            type: Date,
            default: 0
        },
        end_date_epoch: {
            type: Number,
            default: 0
        },
        promotion_desc: {
            type: String,
            default: ''
        },
        offer_price: {
            type: Number,
            default: ''
        },
        creator_name: {
            type: String,
            default: ''
        },
        featured: {
            type: Boolean,
            default: false
        },
        proposed_topic_id:{
            type: String,
            default: ''
        }

    },
        {
            timestamps: true
        });

    s.statics.createTopic = (topicData, callback) => {
        new TopicModels(topicData).save(function (err, response_data) {
            if (err) {
                callback({
                    "response_code": 5005,
                    "response_message": "INTERNAL DB ERROR",
                    "response_data": { err }
                })
            }
            if (!err) {
                callback({
                    "response_code": 2000, "response_message": "Your Topic created successfully.",
                    "response_data": {
                        "user_id": response_data.user_id,
                        "_id": response_data._id,
                        "creator_name": response_data.creator_name,
                        "offer_price": response_data.offer_price,
                        "promotion_desc": response_data.promotion_desc,
                        "end_date": response_data.end_date,
                        "start_date": response_data.start_date,
                        "rating": response_data.rating,
                        "bookings": response_data.bookings,
                        "views": response_data.views,
                        "promoted": response_data.promoted,
                        "topic_mode": response_data.topic_mode,
                        "image_url": config.liveUrl + config.topicImagePath + response_data.image_url,
                        "session_duration": response_data.session_duration,
                        "price": response_data.price,
                        "learn_description": response_data.learn_description,
                        "topic_description": response_data.topic_description,
                        "topic_header": response_data.topic_header,
                        "sub_cat_name": response_data.sub_cat_name,
                        "sub_cat_id": response_data.sub_cat_id,
                        "main_cat_name": response_data.main_cat_name,
                        "main_cat_id": response_data.main_cat_id,
                        "proposed_topic_id":response_data.proposed_topic_id
                    }
                });
            }
        })
    }
    s.statics.editTopic = (topicData, callback) => {
        if (topicData.topic_id) {
            TopicModels.findOne({
                _id: topicData.topic_id,
                topic_mode: 'PROMOTED'
            }, function (err, t) {
                if (t === null) {
                    //do nothing
                    if (!topicData.image_url) {
                        TopicModels.update({
                            _id: topicData.topic_id
                        }, {
                                $set: {
                                    session_duration: topicData.session_duration,
                                    price: topicData.price,
                                    learn_description: topicData.learn_description,
                                    topic_description: topicData.topic_description,
                                    topic_header: topicData.topic_header,
                                    main_cat_id: topicData.main_cat_id,
                                    main_cat_name: topicData.main_cat_name,
                                    sub_cat_id: topicData.sub_cat_id,
                                    sub_cat_name: topicData.sub_cat_name
                                }
                            }).exec(function (err, tu) {
                                if (err) {
                                    callback({
                                        "response_code": 5005,
                                        "response_message": "INTERNAL DB ERROR",
                                        "response_data": {}
                                    })
                                }
                                if (!err) {
                                    if (tu.n === 1 && tu.nModified === 1) {
                                        TopicModels.findOne({ _id: topicData.topic_id }, function (err, response_data) {
                                            if (err) {
                                                callback({
                                                    "response_code": 5005,
                                                    "response_message": "INTERNAL DB ERROR.",
                                                    "response_data": {}
                                                })
                                            }
                                            if (!err) {
                                                //callback({ "response_code":2000,"response_message":"Topic updated.",response_data});
                                                callback({
                                                    "response_code": 2000, "response_message": "Your Topic created successfully.",
                                                    "response_data": {
                                                        "user_id": response_data.user_id,
                                                        "_id": response_data._id,
                                                        "creator_name": response_data.creator_name,
                                                        "offer_price": response_data.offer_price,
                                                        "promotion_desc": response_data.promotion_desc,
                                                        "end_date": response_data.end_date,
                                                        "start_date": response_data.start_date,
                                                        "rating": response_data.rating,
                                                        "bookings": response_data.bookings,
                                                        "views": response_data.views,
                                                        "promoted": response_data.promoted,
                                                        "topic_mode": response_data.topic_mode,
                                                        "image_url": config.liveUrl + config.topicImagePath + response_data.image_url,
                                                        "session_duration": response_data.session_duration,
                                                        "price": response_data.price,
                                                        "learn_description": response_data.learn_description,
                                                        "topic_description": response_data.topic_description,
                                                        "topic_header": response_data.topic_header,
                                                        "sub_cat_name": response_data.sub_cat_name,
                                                        "sub_cat_id": response_data.sub_cat_id,
                                                        "main_cat_name": response_data.main_cat_name,
                                                        "main_cat_id": response_data.main_cat_id,
                                                        "proposed_topic_id":response_data.proposed_topic_id
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }
                            })
                    }
                    if (topicData.image_url) {
                        TopicModels.update({
                            _id: topicData.topic_id
                        }, {
                                $set: {
                                    session_duration: topicData.session_duration,
                                    price: topicData.price,
                                    learn_description: topicData.learn_description,
                                    topic_description: topicData.topic_description,
                                    topic_header: topicData.topic_header,
                                    main_cat_id: topicData.main_cat_id,
                                    main_cat_name: topicData.main_cat_name,
                                    sub_cat_id: topicData.sub_cat_id,
                                    sub_cat_name: topicData.sub_cat_name,
                                    image_url: topicData.image_url
                                }
                            }).exec(function (err, tu) {
                                if (err) {
                                    callback({
                                        "response_code": 5005,
                                        "response_message": "INTERNAL DB ERROR",
                                        "response_data": {}
                                    })
                                }
                                if (!err) {
                                    if (tu.n === 1 && tu.nModified === 1) {
                                        TopicModels.findOne({ _id: topicData.topic_id }, function (err, response_data) {
                                            if (err) {
                                                callback({
                                                    "response_code": 5005,
                                                    "response_message": "INTERNAL DB ERROR.",
                                                    "response_data": {}
                                                })
                                            }
                                            if (!err) {
                                                //callback({ "response_code":2000,"response_message":"Topic updated.",response_data});
                                                callback({
                                                    "response_code": 2000, "response_message": "Your Topic created successfully.",
                                                    "response_data": {
                                                        "user_id": response_data.user_id,
                                                        "_id": response_data._id,
                                                        "creator_name": response_data.creator_name,
                                                        "offer_price": response_data.offer_price,
                                                        "promotion_desc": response_data.promotion_desc,
                                                        "end_date": response_data.end_date,
                                                        "start_date": response_data.start_date,
                                                        "rating": response_data.rating,
                                                        "bookings": response_data.bookings,
                                                        "views": response_data.views,
                                                        "promoted": response_data.promoted,
                                                        "topic_mode": response_data.topic_mode,
                                                        "image_url": config.liveUrl + config.topicImagePath + response_data.image_url,
                                                        "session_duration": response_data.session_duration,
                                                        "price": response_data.price,
                                                        "learn_description": response_data.learn_description,
                                                        "topic_description": response_data.topic_description,
                                                        "topic_header": response_data.topic_header,
                                                        "sub_cat_name": response_data.sub_cat_name,
                                                        "sub_cat_id": response_data.sub_cat_id,
                                                        "main_cat_name": response_data.main_cat_name,
                                                        "main_cat_id": response_data.main_cat_id,
                                                        "proposed_topic_id":response_data.proposed_topic_id
                                                    }
                                                });
                                            }
                                        });
                                        // callback({
                                        //     "response_code": 2000,
                                        //     "response_message": "Topic updated.",
                                        //     "response_data": {}
                                        // })
                                    }
                                }
                            })
                    }
                }
                if (t != null) {
                    TopicModels.update({
                        _id: topicData.topic_id
                    }, {
                            $set: {
                                topic_mode: 'ACTIVE',
                                promoted: false
                            }
                        }).exec(function (err, u) {
                            //do nothing
                            if (!topicData.image_url) {
                                TopicModels.update({
                                    _id: topicData.topic_id
                                }, {
                                        $set: {
                                            session_duration: topicData.session_duration,
                                            price: topicData.price,
                                            learn_description: topicData.learn_description,
                                            topic_description: topicData.topic_description,
                                            topic_header: topicData.topic_header,
                                            main_cat_id: topicData.main_cat_id,
                                            main_cat_name: topicData.main_cat_name,
                                            sub_cat_id: topicData.sub_cat_id,
                                            sub_cat_name: topicData.sub_cat_name
                                        }
                                    }).exec(function (err, tu) {
                                        if (err) {
                                            callback({
                                                "response_code": 5005,
                                                "response_message": "INTERNAL DB ERROR",
                                                "response_data": {}
                                            })
                                        }
                                        if (!err) {
                                            if (tu.n === 1 && tu.nModified === 1) {
                                                TopicModels.findOne({ _id: topicData.topic_id }, function (err, response_data) {
                                                    if (err) {
                                                        callback({
                                                            "response_code": 5005,
                                                            "response_message": "INTERNAL DB ERROR.",
                                                            "response_data": {}
                                                        })
                                                    }
                                                    if (!err) {
                                                        //callback({ "response_code":2000,"response_message":"Topic updated.",response_data});
                                                        callback({
                                                            "response_code": 2000, "response_message": "Your Topic created successfully.",
                                                            "response_data": {
                                                                "user_id": response_data.user_id,
                                                                "_id": response_data._id,
                                                                "creator_name": response_data.creator_name,
                                                                "offer_price": response_data.offer_price,
                                                                "promotion_desc": response_data.promotion_desc,
                                                                "end_date": response_data.end_date,
                                                                "start_date": response_data.start_date,
                                                                "rating": response_data.rating,
                                                                "bookings": response_data.bookings,
                                                                "views": response_data.views,
                                                                "promoted": response_data.promoted,
                                                                "topic_mode": response_data.topic_mode,
                                                                "image_url": config.liveUrl + config.topicImagePath + response_data.image_url,
                                                                "session_duration": response_data.session_duration,
                                                                "price": response_data.price,
                                                                "learn_description": response_data.learn_description,
                                                                "topic_description": response_data.topic_description,
                                                                "topic_header": response_data.topic_header,
                                                                "sub_cat_name": response_data.sub_cat_name,
                                                                "sub_cat_id": response_data.sub_cat_id,
                                                                "main_cat_name": response_data.main_cat_name,
                                                                "main_cat_id": response_data.main_cat_id,
                                                                "proposed_topic_id":response_data.proposed_topic_id
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        }
                                    })
                            }
                            if (topicData.image_url) {
                                TopicModels.update({
                                    _id: topicData.topic_id
                                }, {
                                        $set: {
                                            session_duration: topicData.session_duration,
                                            price: topicData.price,
                                            learn_description: topicData.learn_description,
                                            topic_description: topicData.topic_description,
                                            topic_header: topicData.topic_header,
                                            main_cat_id: topicData.main_cat_id,
                                            main_cat_name: topicData.main_cat_name,
                                            sub_cat_id: topicData.sub_cat_id,
                                            sub_cat_name: topicData.sub_cat_name,
                                            image_url: topicData.image_url
                                        }
                                    }).exec(function (err, tu) {
                                        if (err) {
                                            callback({
                                                "response_code": 5005,
                                                "response_message": "INTERNAL DB ERROR",
                                                "response_data": {}
                                            })
                                        }
                                        if (!err) {
                                            if (tu.n === 1 && tu.nModified === 1) {
                                                TopicModels.findOne({ _id: topicData.topic_id }, function (err, response_data) {
                                                    if (err) {
                                                        callback({
                                                            "response_code": 5005,
                                                            "response_message": "INTERNAL DB ERROR.",
                                                            "response_data": {}
                                                        })
                                                    }
                                                    if (!err) {
                                                        //callback({ "response_code":2000,"response_message":"Topic updated.",response_data});
                                                        callback({
                                                            "response_code": 2000, "response_message": "Your Topic created successfully.",
                                                            "response_data": {
                                                                "user_id": response_data.user_id,
                                                                "_id": response_data._id,
                                                                "creator_name": response_data.creator_name,
                                                                "offer_price": response_data.offer_price,
                                                                "promotion_desc": response_data.promotion_desc,
                                                                "end_date": response_data.end_date,
                                                                "start_date": response_data.start_date,
                                                                "rating": response_data.rating,
                                                                "bookings": response_data.bookings,
                                                                "views": response_data.views,
                                                                "promoted": response_data.promoted,
                                                                "topic_mode": response_data.topic_mode,
                                                                "image_url": config.liveUrl + config.topicImagePath + response_data.image_url,
                                                                "session_duration": response_data.session_duration,
                                                                "price": response_data.price,
                                                                "learn_description": response_data.learn_description,
                                                                "topic_description": response_data.topic_description,
                                                                "topic_header": response_data.topic_header,
                                                                "sub_cat_name": response_data.sub_cat_name,
                                                                "sub_cat_id": response_data.sub_cat_id,
                                                                "main_cat_name": response_data.main_cat_name,
                                                                "main_cat_id": response_data.main_cat_id,
                                                                "proposed_topic_id":response_data.proposed_topic_id
                                                            }
                                                        });
                                                    }
                                                });
                                                // callback({
                                                //     "response_code": 2000,
                                                //     "response_message": "Topic updated.",
                                                //     "response_data": {}
                                                // })
                                            }
                                        }
                                    })
                            }

                        })
                }
            })
        }
    }
    s.statics.viewTopics = function (topicDetails, callback) {
        TopicModels.findOne(
            { user_id: topicDetails.user_id },
            function (err, res) {
                if (err) {
                    console.log("error");
                }
                if (!err) {
                    if (topicDetails.user_type == 'Tutor' && topicDetails.topic_mode == 'ALL') {
                        TopicModels.find(
                            { user_id: topicDetails.user_id },
                            function (err, res) {
                                if (err) {
                                    console.log("error");
                                }
                                if (!err) {
                                    async.forEach(res, function (item, callback) {
                                        item.image_url = item.image_url ? config.liveUrl + config.topicImagePath + item.image_url : '';
                                        //console.log(item.category_image);
                                        callback();
                                    }, function (err) {
                                        if (err) {
                                            console.log(err);
                                        }
                                        if (!err) {
                                            console.log("complete");
                                            callback({ "response_code": 2000, "result": res });
                                        }
                                    });
                                }
                            }).sort({ "createdAt": -1 });
                    }
                    if (topicDetails.user_type == 'Tutor' && topicDetails.topic_mode != 'ALL') {
                        if(topicDetails.topic_mode == "ACTIVE"){
                           TopicModels.find(
                            { user_id: topicDetails.user_id, topic_mode: { $in: [topicDetails.topic_mode, 'PROMOTED'] } },
                            function (err, res) {
                                if (err) {
                                    console.log("error");
                                }
                                if (!err) {
                                    async.forEach(res, function (item, callback) {
                                        item.image_url = item.image_url ? config.liveUrl + config.topicImagePath + item.image_url : '';
                                        //console.log(item.category_image);
                                        callback();
                                    }, function (err) {
                                        if (err) {
                                            console.log(err);
                                        }
                                        if (!err) {
                                            console.log("complete");
                                            callback({ "response_code": 2000, "result": res });
                                        }
                                    });
                                }
                            }).sort({ "createdAt": -1 }); 
                        }
                        if(topicDetails.topic_mode == "PROMOTED"){
                            TopicModels.find(
                            { user_id: topicDetails.user_id, topic_mode: { $in: ['PROMOTED'] } },
                            function (err, res) {
                                if (err) {
                                    console.log("error");
                                }
                                if (!err) {
                                    async.forEach(res, function (item, callback) {
                                        item.image_url = item.image_url ? config.liveUrl + config.topicImagePath + item.image_url : '';
                                        //console.log(item.category_image);
                                        callback();
                                    }, function (err) {
                                        if (err) {
                                            console.log(err);
                                        }
                                        if (!err) {
                                            console.log("complete");
                                            callback({ "response_code": 2000, "result": res });
                                        }
                                    });
                                }
                            }).sort({ "createdAt": -1 });
                        }
                        if(topicDetails.topic_mode == "DISABLE"){
                            TopicModels.find(
                            { user_id: topicDetails.user_id, topic_mode: { $in: ['DISABLE'] } },
                            function (err, res) {
                                if (err) {
                                    console.log("error");
                                }
                                if (!err) {
                                    async.forEach(res, function (item, callback) {
                                        item.image_url = item.image_url ? config.liveUrl + config.topicImagePath + item.image_url : '';
                                        //console.log(item.category_image);
                                        callback();
                                    }, function (err) {
                                        if (err) {
                                            console.log(err);
                                        }
                                        if (!err) {
                                            console.log("complete");
                                            callback({ "response_code": 2000, "result": res });
                                        }
                                    });
                                }
                            }).sort({ "createdAt": -1 });
                        }
                        if(topicDetails.topic_mode == "UNDERREVIEW"){
                            TopicModels.find(
                            { user_id: topicDetails.user_id, topic_mode: { $in: ['UNDERREVIEW'] } },
                            function (err, res) {
                                if (err) {
                                    console.log("error");
                                }
                                if (!err) {
                                    async.forEach(res, function (item, callback) {
                                        item.image_url = item.image_url ? config.liveUrl + config.topicImagePath + item.image_url : '';
                                        //console.log(item.category_image);
                                        callback();
                                    }, function (err) {
                                        if (err) {
                                            console.log(err);
                                        }
                                        if (!err) {
                                            console.log("complete");
                                            callback({ "response_code": 2000, "result": res });
                                        }
                                    });
                                }
                            }).sort({ "createdAt": -1 });
                        }
                        
                    }
                    if (topicDetails.user_type == 'Learner') {
                        TopicModels.find(
                            { user_id: topicDetails.user_id, topic_mode: { $in: ['ACTIVE'] } },
                            function (err, res) {
                                if (err) {
                                    console.log("error");
                                }
                                if (!err) {
                                    async.forEach(res, function (item, callback) {
                                        item.image_url = item.image_url ? config.liveUrl + config.topicImagePath + item.image_url : '';
                                        //console.log(item.category_image);
                                        callback();
                                    }, function (err) {
                                        if (err) {
                                            console.log(err);
                                        }
                                        if (!err) {
                                            console.log("complete");
                                            callback({ "response_code": 2000, "result": res });
                                        }
                                    });
                                }
                            }).sort({ "createdAt": -1 });
                    }

                }
            });
    }
    s.statics.editDisableTopic = function (topicData, callback) {
        TopicModels.findOne({
            _id: topicData.topic_id
        }, function (err, getTopicData) {
            if (err) {
                console.log(err);
            }
            if (!err) {
                if (getTopicData.topic_mode == 'ACTIVE') {
                    TopicModels.update({
                        _id: topicData.topic_id
                    }, {
                            $set: {
                                topic_mode: 'DISABLE',
                            }
                        }).exec(function (err, tu) {
                            if (err) {
                                callback({
                                    "response_code": 5005,
                                    "response_message": "INTERNAL DB ERROR",
                                    "response_data": {}
                                })
                            }
                            if (!err) {
                                if (tu.n === 1 && tu.nModified === 1) {
                                    callback({
                                        "response_code": 2000,
                                        "response_message": "Topic updated.",
                                        "response_data": {}
                                    })
                                }
                            }
                        });
                }
                if (getTopicData.topic_mode == 'PROMOTED') {

                    TopicModels.update({
                        _id: topicData.topic_id
                    }, {
                            $set: {
                                topic_mode: 'DISABLE',
                                promoted: false
                            }
                        }).exec(function (err, tu) {
                            if (err) {
                                callback({
                                    "response_code": 5005,
                                    "response_message": "INTERNAL DB ERROR",
                                    "response_data": {}
                                })
                            }
                            if (!err) {
                                if (tu.n === 1 && tu.nModified === 1) {
                                    callback({
                                        "response_code": 2000,
                                        "response_message": "Topic updated.",
                                        "response_data": {}
                                    })
                                }
                            }
                        });
                }
                if (getTopicData.topic_mode == 'DISABLE') {
                    TopicModels.update({
                        _id: topicData.topic_id
                    }, {
                            $set: {
                                topic_mode: 'ACTIVE',
                            }
                        }).exec(function (err, tu) {
                            if (err) {
                                callback({
                                    "response_code": 5005,
                                    "response_message": "INTERNAL DB ERROR",
                                    "response_data": {}
                                })
                            }
                            if (!err) {
                                if (tu.n === 1 && tu.nModified === 1) {
                                    callback({
                                        "response_code": 2000,
                                        "response_message": "Topic updated.",
                                        "response_data": {}
                                    })
                                }
                            }
                        });
                }
            }
        });
    }
    s.statics.normalSearch = function (searchData, callback) {
        TopicModels.find({ $text: { $search: searchData.search_text } , topic_mode: "ACTIVE" }).exec(function (err, result) {
            if (err) {
                callback({
                    "response_code": 5005,
                    "response_message": "INTERNAL DB ERROR",
                    "response_data": { err }
                })
            }
            if (!err) {
                async.forEach(result, function (item, callback) {
                    item.image_url = item.image_url ? config.liveUrl + config.topicImagePath + item.image_url : '';
                    //console.log(item.category_image);
                    callback();
                }, function (err) {
                    if (err) {
                        console.log(err);
                    }
                    if (!err) {
                        console.log("complete");
                        callback({ "response_code": 2000, "response_data": result });
                    }
                });
            }
        })
    }
    s.statics.promoteTopic = function (topicData, callback) {
        console.log(topicData);
        TopicModels.find({ _id: topicData.topic_id }, function (err, res) {
            if (err) {
                console.log("error");
            }
            if (!err) {
                if (res.length > 0) {
                    TopicModels.update({
                        _id: topicData.topic_id
                    }, {
                            $set: {
                                start_date: topicData.start_date,
                                end_date: topicData.end_date,
                                offer_price: topicData.offer_price,
                                promotion_desc: topicData.promotion_desc,
                                topic_mode: 'PROMOTED',
                                promoted: true
                            }
                        }).exec(function (err, updated) {
                            if (err) {
                                callback({
                                    "response_code": 5005,
                                    "response_message": "INTERNAL DB ERROR",
                                    "response_data": {}
                                })
                            }
                            if (!err) {
                                if (updated.n === 1 && updated.nModified === 1) {
                                    callback({
                                        "response_code": 2000,
                                        "response_message": "Succesfully Promoted.",
                                        "response_data": {}
                                    })
                                }
                            }
                        })
                }
            }
        });
    }
    s.statics.getTopics = function (noDataToCome, callback) {
        TopicModels.find({ promoted: true, end_date: { $gt: new Date().getTime() } }).exec(function (err, getAllData) {
            if (err) {
                console.log(err);
            }
            if (!err) {
                callback(getAllData);
            }
        });
    }
    s.statics.updateTopicData = function (dataToUpdate, callback) {
        async.forEach(dataToUpdate, function (item, callback) {
            TopicModels.update(
                { _id: item._id, promoted: true },
                {
                    $set:
                    {
                        promoted: false,
                        topic_mode: 'ACTIVE'
                    }
                }).exec(function (err, updated) {
                    if (err) {
                        console.log(err);
                    }
                    if (!err) {
                        console.log(updated);
                    }
                });
            callback();
        }, function (err) {
            if (err) {
                console.log(err);
            }
            if (!err) {
                console.log("complete");
                callback({ "response_code": 2000, "response_data": "done" });
            }
        });
    }
    s.statics.updateBookingCount = function (topic_id, callback) {
        TopicModels.update({
            _id: topic_id
        }, { $inc: { bookings: 1 } }).exec(function (err, b) {
            if (err) {
                console.log(err);
            }
            if (!err) {
                if (b.n === 1 && b.nModified === 1) {
                    callback({ "response_code": 2000, "response_message": "Sucessfully saved.", "response_data": {} });
                }
                else {
                    callback({ "response_code": 2000, "response_message": "Sucessfully saved.", "response_data": {} });
                }
            }
        })
    }
    s.statics.delTopic = function (topicD, callback) {
        TopicModels.remove({
            _id: topicD.topic_id,
            user_id: topicD.user_id
        }, function (err, d) {
            if (err) {
                callback({ "response_code": 5005, "response_message": "INTERNAL DB ERROR", "response_data": { err } });
            }
            if (!err) {
                if (d.result.n === 1 && d.result.ok === 1) {
                    callback({ "response_code": 2000, "response_message": "Topic deleted successfully.", "response_data": {} });
                }
                if (d.result.n === 0) {
                    callback({ "response_code": 5005, "response_message": "Unsuccessful.", "response_data": {} });
                }
            }
        })
    }
    s.statics.dashboardCounts = function (data, callback) {
        async.parallel([
            function (callback) {
                TopicModels.find({
                    topic_mode: 'ACTIVE',
                    user_id: data.user_id
                }).count(callback)

            },
            function (callback) {
                TopicModels.find({
                    topic_mode: 'PROMOTED',
                    user_id: data.user_id
                }).count(callback)

            },
            function (callback) {
                TopicModels.find({
                    topic_mode: 'UNDERREVIEW',
                    user_id: data.user_id
                }).count(callback)
            }
        ], function (err, data) {
            if (err) {
                callback({
                    "response_code": 5005,
                    "response_message": "INTERNAL DB ERROR",
                    "response_data": { err }
                })
            }
            if (!err) {
                callback(data);
            }
        })
    }
    s.statics.getTopicData = function (topicdata , callback){
       TopicModels.findOne({
        _id: topicdata.topic_id
       }, function(err, data){ //console.log(data); return;
            if(err){
                callback({
                        "response_code": 5005,
                        "response_message": "INTERNAL DB ERROR",
                        "response_data": err
                    })
            }
            if(!err){
                callback({ "response_code": 2000, "result": data});
            }
       }) 
    }
    s.statics.getTopicByCategory = function (topicdata , callback){
        
        TopicModels.aggregate([
                {
                    $match: {
                        "main_cat_id": topicdata.cat_id
                    }
                }, {
                    $sort: {updatedAt: -1}
                },
                {
                    $lookup: {
                        from: "users",
                        localField: "user_id",
                        foreignField: "_id",
                        as: "user_details"
                    }
                },
                {
                    $unwind: "$user_details"
                }, {
                    $project: {
                        "user_id": 1,
                        "main_cat_id": 1,
                        "main_cat_name": 1,
                        "sub_cat_id": 1,
                        "sub_cat_name": 1,
                        "topic_header": 1,
                        "topic_description": 1,
                        "learn_description": 1,
                        "price":1,
                        "session_duration": 1,
                        "image_url": 1,
                        "topic_mode": 1,
                        "promoted": 1,
                        "views": 1,
                        "bookings": 1,
                        "rating": 1,
                        "start_date": 1,
                        "end_date": 1,
                        "promotion_desc":1,
                        "offer_price":1,
                        "creator_name":1,
                        "fname": "$user_details.fname",
                        "lname": "$user_details.lname"
                    }
                }
            ], function (err, res) { //console.log(res); return;
                if (err) {
                    callback({
                        "response_code": 5005,
                        "response_message": "INTERNAL DB ERROR",
                        "response_data": err
                    })
                }
                if (!err) {
                    callback({ "response_code": 2000, "result": res});
                }
            })
         
    }
    
    s.statics.getTopicDetails = function (topicdata , callback){
        TopicModels.aggregate([
                {
                    $match: {
                        "_id": topicdata.topic_id
                    }
                }, {
                    $sort: {updatedAt: -1}
                },
                {
                    $lookup: {
                        from: "users",
                        localField: "user_id",
                        foreignField: "_id",
                        as: "user_details"
                    }
                },
                {
                    $unwind: "$user_details"
                }, {
                    $project: {
                        "user_id": 1,
                        "main_cat_id": 1,
                        "main_cat_name": 1,
                        "sub_cat_id": 1,
                        "sub_cat_name": 1,
                        "topic_header": 1,
                        "topic_description": 1,
                        "learn_description": 1,
                        "price":1,
                        "session_duration": 1,
                        "image_url": 1,
                        "topic_mode": 1,
                        "promoted": 1,
                        "views": 1,
                        "bookings": 1,
                        "rating": 1,
                        "start_date": 1,
                        "end_date": 1,
                        "promotion_desc":1,
                        "offer_price":1,
                        "creator_name":1,
                        "fname": "$user_details.fname",
                        "lname": "$user_details.lname",
                        /*"creator_image":"$user_details.image_url",*/
                        "creator_id":"$user_details._id"
                    }
                }
            ], function (err, res) { //console.log(res); return;
                if (err) {
                    callback({
                        "response_code": 5005,
                        "response_message": "INTERNAL DB ERROR",
                        "response_data": err
                    })
                }
                if (!err) {
                    if(res.length > 0){
                        callback({ "response_code": 2000, "result": res});
                    }else{
                        callback({ "response_code": 2001, "result": []});
                    }
                }
            })
    }
    
    /*s.statics.getLimitedTopics = function (topicdata , callback){
        
        var rightNow = new Date().getTime();
        
        TopicModels.aggregate([
                {
                    $match: {
                        $and:
                        [
                            {"user_id": topicdata.other_profile_id},
                            {"start_date": {$lte: rightNow}}
                        ]
                        
                    }
                }, {
                    $sort: {start_date: 1}
                },
                { $limit : 5 }
            ], function (err, res) { //console.log(res); return;
                if (err) {
                    callback({
                        "response_code": 5005,
                        "response_message": "INTERNAL DB ERROR",
                        "response_data": err
                    })
                }
                if (!err) {
                    if(res.length > 0){
                        callback({ "response_code": 2000, "result": res});
                    }else{
                        callback({ "response_code": 2001, "result": []});
                    }
                }
            })
    }*/
                                                                                    
    s.statics.getOfferList = function (noDataToCome, callback) {
        
        TopicModels.aggregate([
                {
                    $match: {
                        $and:
                        [
                            {promoted: true},
                            {end_date: { $gt: new Date() }}
                        ]
                        
                    }
                }, {
                    $sort: {start_date: 1}
                }
            ], function (err, res) { //console.log(res); return;
                if (err) {
                    callback({
                        "response_code": 5005,
                        "response_message": "INTERNAL DB ERROR",
                        "response_data": err
                    })
                }
                if (!err) {
                    if(res.length > 0){
                        callback({ "response_code": 2000, "result": res});
                    }else{
                        callback({ "response_code": 2001, "result": []});
                    }
                }
            })
    }   
    
    s.statics.getFeaturedTopic = function (topicdata , callback){
        
        TopicModels.aggregate([
                {
                    $match: {
                        "featured": true
                    }
                }, {
                    $sort: {updatedAt: -1}
                },
                {
                    $lookup: {
                        from: "users",
                        localField: "user_id",
                        foreignField: "_id",
                        as: "user_details"
                    }
                },
                {
                    $unwind: "$user_details"
                }, {
                    $project: {
                        "user_id": 1,
                        "main_cat_id": 1,
                        "main_cat_name": 1,
                        "sub_cat_id": 1,
                        "sub_cat_name": 1,
                        "topic_header": 1,
                        "topic_description": 1,
                        "learn_description": 1,
                        "creator_name":1,
                        "fname": "$user_details.fname",
                        "lname": "$user_details.lname",
                        "image_url":"$user_details.image_url"
                    }
                }
            ], function (err, res) { //console.log(res); return;
                if (err) {
                    callback({
                        "response_code": 5005,
                        "response_message": "INTERNAL DB ERROR",
                        "response_data": err
                    })
                }
                if (!err) {
                    async.forEach(res, function (item, callback) {
                        item.image_url = item.image_url ? config.liveUrl + config.profilepicPath + item.image_url : '';
                        callback();
                    }, function (err) {
                        if (err) {
                                console.log(err);
                        }
                        if(!err){
                                
                        callback({ "response_code":2000,"result":res});
                        }
                    });
                }
            })
    }
    
    s.statics.spotLightFeaturedTopic = function (topicdata , callback){
        
        TopicModels.aggregate([
                {
                    $match: {
                        "featured": true
                    }
                }, {
                    $sort: {updatedAt: -1}
                },
                { $limit : 5 },
                {
                    $lookup: {
                        from: "users",
                        localField: "user_id",
                        foreignField: "_id",
                        as: "user_details"
                    }
                },
                {
                    $unwind: "$user_details"
                }, {
                    $project: {
                        "user_id": 1,
                        "main_cat_id": 1,
                        "main_cat_name": 1,
                        "sub_cat_id": 1,
                        "sub_cat_name": 1,
                        "topic_header": 1,
                        "topic_description": 1,
                        "learn_description": 1,
                        "creator_name":1,
                        "fname": "$user_details.fname",
                        "lname": "$user_details.lname",
                        "image_url":"$user_details.image_url"
                    }
                }
            ], function (err, res) { //console.log(res); return;
                if (err) {
                    callback({
                        "response_code": 5005,
                        "response_message": "INTERNAL DB ERROR",
                        "response_data": err
                    })
                }
                if (!err) {
                    async.forEach(res, function (item, callback) {
                        item.image_url = item.image_url ? config.liveUrl + config.profilepicPath + item.image_url : '';
                        callback();
                    }, function (err) {
                        if (err) {
                                console.log(err);
                        }
                        if(!err){
                                
                        callback({ "response_code":2000,"result":res});
                        }
                    });
                }
            })
    }
    s.statics.spotLightOfferList = function (noDataToCome, callback) {
        
        TopicModels.aggregate([
                {
                    $match: {
                        $and:
                        [
                            {promoted: true},
                            {end_date: { $gt: new Date() }}
                        ]
                        
                    }
                }, 
                { $limit : 4 },
                {
                    $sort: {start_date: 1}
                }
            ], function (err, res) { //console.log(res); return;
                if (err) {
                    callback({
                        "response_code": 5005,
                        "response_message": "INTERNAL DB ERROR",
                        "response_data": err
                    })
                }
                if (!err) {
                    if(res.length > 0){
                        
                        async.forEach(res, function (item, callback) {
                            item.image_url = item.image_url ? config.liveUrl + config.topicImagePath + item.image_url : '';
                            item.start_date = new Date(item.start_date).getTime();
                            item.end_date = new Date(item.end_date).getTime();
                            callback();
                        }, function (err) {
                            if (err) {
                                    console.log(err);
                            }
                            if(!err){

                            callback({ "response_code":2000,"result":res});
                            }
                        }); 
                    }else{
                        callback({ "response_code": 2001, "result": []});
                    }
                }
            })
    }   
    s.statics.promotedList = function(promoteData, callback){ //console.log("inn"); return;
        var mew = [];
        TopicModels.find(
            { user_id: promoteData.user_id, topic_mode: { $in: ['PROMOTED'] } },
            function (err, res) {
                if (err) {
                    console.log("error");
                }
                if (!err) {
                    async.forEach(res, function (item, callback) { 
                        item.image_url = item.image_url ? config.liveUrl + config.topicImagePath + item.image_url : '';
                        item.start_date_epoch = new Date(item.start_date).getTime();
                        item.end_date_epoch = new Date(item.end_date).getTime();
                        //console.log(item.start_date1); return;
                        callback();
                    }, function (err) { //console.log(res); return;
                        if (err) {
                            console.log(err);
                        }
                        if (!err) {
                            if(res.length > 0){
                                callback({ "response_code": 2000, "result": res });
                            }else{
                                callback({ "response_code": 2001, "result": [] });
                            }
                        }
                    });
                }
            }).sort({ "createdAt": -1 });
    }
    s.statics.alreadyAccepted = function (proposed_id, user_id, callback){
        TopicModels.findOne({
            user_id: user_id,
            proposed_topic_id: proposed_id
        }, function (err, u) { //console.log(u); return;

            if (err) {
                callback({
                    "response_code": 5005,
                    "response_message": "INTERNAL DB ERROR",
                    "response_data": {}
                })
            }
            if (!err) {
                
                if(u === null){
                    callback({
                        "response_code": 2001,
                        "response_message": "Not Exist",
                        "response_data": {}
                    })
                }
                else{
                    callback({
                        "response_code": 2000,
                        "response_message": "Exist",
                        "response_data": {}
                    })
                }

            }
        })
    }
    s.statics.acceptedList = function (acceptedData, callback){
        TopicModels.aggregate([
        {
            $match: {
                "proposed_topic_id": acceptedData.proposed_topic_id
            }
        }, {
            $sort: {updatedAt: -1}
        },
        {
            $lookup: {
                from: "users",
                localField: "user_id",
                foreignField: "_id",
                as: "user_details"
            }
        },
        {
            $unwind: "$user_details"
        },
        {
            $project: {
                "user_id": 1,
                "main_cat_id": 1,
                "main_cat_name": 1,
                "sub_cat_id": 1,
                "sub_cat_name": 1,
                "creator_name":1,
                "fname": "$user_details.fname",
                "lname": "$user_details.lname",
                "image_url":"$user_details.image_url"
            }
        }
    ], function (err, res) { //console.log(res); return;
        if (err) {
            callback({
                "response_code": 5005,
                "response_message": "INTERNAL DB ERROR",
                "response_data": err
            })
        }
        if (!err) {
            callback({ "response_code": 2000, "result": res});
        }
    })
    }
    s.statics.deletePromotion = function(promotionData, callback){
        TopicModels.update({
            _id: promotionData.topic_id
        }, {
                $set: {
                    topic_mode: 'ACTIVE',
                    promoted: false,
                    start_date: 0,
                    end_date: 0,
                    offer_price: 0,
                    promotion_desc: ""
                }
            }).exec(function (err, tu) {
                if (err) {
                    callback({
                        "response_code": 5005,
                        "response_message": "INTERNAL DB ERROR",
                        "response_data": {}
                    })
                }
                if (!err) {
                    if (tu.n === 1 && tu.nModified === 1) {
                        callback({
                            "response_code": 2000,
                            "response_message": "Topic updated.",
                            "response_data": {}
                        })
                    }
                }
            });
    }
    s.statics.updateViewCount = function(topic_id, updated_count, callback){
        TopicModels.update({
            _id: topic_id
        }, {
                $set: {
                    views: updated_count
                }
            }).exec(function (err, tu) {
                if (err) {
                    callback({
                        "response_code": 5005,
                        "response_message": "INTERNAL DB ERROR",
                        "response_data": {}
                    })
                }
                if (!err) {
                    if (tu.n === 1 && tu.nModified === 1) {
                        callback({
                            "response_code": 2000,
                            "response_message": "Topic updated.",
                            "response_data": {}
                        })
                    }
                }
            });
    }
    
    return s;
}());

module.exports = TopicModels;