var mongoose = require("mongoose");
var bcrypt = require('bcrypt-nodejs');
var config = require('../config');
var BookingModels = mongoose.model("Bookings", function () {
    var s = new mongoose.Schema({
        _id: {
            type: String,
            required: true,
        },
        user_type:{
            type:String,
            default:''
        },
        user_id:{
            type:String,
            required:true
        },
        topic_id:{
            type:String,
            required:true
        },
        session_date:{
            type:Number,
            default:''
        },
        session_time:{
            type:String,
            default:''
        },
        payment_method:{
            type:String,
            default:''
        },
        payment_stat:{
          type:Boolean,
          default:false  
        },
        payment_time: {
            type: Number,
            default: 0
        },
        payment_status:{
            type:String,
            default: ''
        },
        message:{
            type:String,
            default:''
        },
        transaction_id:{
            type:String,
            default:''
        },
        issaved:{
            type:Boolean,
            default:false
        },
        isbooked:{
            type:Boolean,
            default:false
        },
        isreported:{
            type:Boolean,
            default:false
        }
    })
    
    s.statics.alreadySavedData = function(topicData,callback){
        BookingModels.findOne({
            user_id : topicData.user_id,
            topic_id:topicData.topic_id,
            issaved:true
        },function(err,r){
            if(err) console.log(err);
            else{
                callback(r);
            }
        })
    }

    s.statics.saveTopic = function (saveData, callback) {
        BookingModels.getTopicData(saveData, function (tD) {
            if (tD === null) {
                saveData.issaved = true;
                new BookingModels(saveData).save(function (err, s) {
                    if(err){
                        callback({
                            "response_code": 5005,
                            "response_message": "INTERNAL DB ERROR",
                            "response_data": {err}
                        })
                    }
                    if(!err){
                        callback({
                            "response_code": 2000,
                            "response_message": "Sucessfully saved.",
                            "response_data": {}
                        })
                    }
                })
            }
            else {
                
                BookingModels.alreadySavedData(saveData, function (savedRec) {
                    if(savedRec === null){
                        /******update existing*****/
                        
                            var booking_id = tD._id;
                            
                            BookingModels.update({
                                _id: booking_id
                            }, {
                                $set: {
                                    issaved: true
                                }
                            }, function (err, v) {
                                if (err)
                                    callback({
                                        "response_code": 5005,
                                        "response_message": "INTERNAL DB ERROR",
                                        "response_data": {err}
                                    })
                                if (!err) {
                                    if (v.nModified === 1) {
                                        callback({
                                            "response_code": 2000,
                                            "response_message": "Sucessfully saved.",
                                            "response_data": {}
                                        })
                                    }
                                }
                            })
                            
                        
                    }else{
                        callback({
                            "response_code": 5000,
                            "response_message": "you have already saved this topic",
                            "response_data": {}
                        });
                    }
                })
                
                
                
            }
        })

    }
    s.statics.getTopicData = function(topicData,callback){
        BookingModels.findOne({
            user_id : topicData.user_id,
            topic_id:topicData.topic_id
        },function(err,r){
            if(err) console.log(err);
            else{
                callback(r);
            }
        })
    }
    s.statics.bookingTopic = function (bookingData, callback) {
        //BookingModels.getTopicData(bookingData, function (tD) {
           bookingData.isbooked = true;
           bookingData.session_date = new Date(bookingData.session_date).getTime();
           bookingData.session_time = new Date(bookingData.session_time).getTime();
           if(bookingData.payment_method == 'card'){
               bookingData.payment_status = "DONE";
           }
           if(bookingData.payment_method !== 'card'){
               bookingData.payment_status = "HOLD";
           }
           new BookingModels(bookingData).save(function(err,b){
            if(err){
                callback({"response_code": 5005,"response_message": "INTERNAL DB ERROR","response_data": {err}});
            }
            if (!err) {
                callback({ "response_code": 2000, "response_message": "Sucessfully saved.", "response_data": {}});
            }
           })
            
        //})
    }
    s.statics.getTopicDetails = function(userData,callback){
        BookingModels.getTopicData(userData,function(tD){
            if(tD===null){
                //the user dsnt have any booking associated with this topic

            }
            else {
                //join with topic table and send the data
                BookingModels.aggregate([
                    {
                        $match:{
                            $and :[
                            {user_id: userData.user_id},
                            {topic_id: userData.topic_id},
                        ]       
                        }
                    },{
                        $lookup: {
                            from: "topics",
                            localField: "topic_id",
                            foreignField: "_id",
                            as: "topicData"
                        }
                    },{
                        $unwind: "$topicData"
                    },{
                        $project: {
                            "booking_status": 1,
                            "payment_stat":1,
                            "creator_name":"$topicData.creator_name",
                            "creator_id":"$topicData.user_id",
                            "offer_price": "$topicData.offer_price",
                            "promotion_desc": "$topicData.promotion_desc",
                            "end_date": "$topicData.end_date",
                            "start_date": "$topicData.start_date",
                            "bookings": "$topicData.bookings",
                            "views": "$topicData.views",
                            "rating":"$topicData.rating",
                            "promoted": "$topicData.promoted",
                            "topic_mode": "$topicData.topic_mode",
                            "image_url": "$topicData.image_url",
                            "session_duration": "$topicData.session_duration",
                            "price": "$topicData.price",
                            "learn_description": "$topicData.learn_description",
                            "topic_description": "$topicData.topic_description",
                            "topic_header": "$topicData.topic_header",
                            "sub_cat_name": "$topicData.sub_cat_name",
                            "sub_cat_id": "$topicData.sub_cat_id",
                            "main_cat_name": "$topicData.main_cat_name",
                            "main_cat_id": "$topicData.main_cat_id"

                        }
                    }
                ],function(err,bData){
                    if(err){
                     callback({"response_code": 5005,"response_message": "INTERNAL DB ERROR","response_data": {err}
                        })   
                    }
                    if(!err){
                        //config.liveUrl + config.topicImagePath +
                        callback({
                            "response_code": 2000,
                            "response_message": "Sucess.",
                            "response_data": {
                                "topic_details": {
                                    "booking_status":bData[0].booking_status,
                                    "creator_id": bData[0].creator_id,
                                    "creator_name": bData[0].creator_name,
                                    "isreported": false,
                                    "offer_price": bData[0].offer_price,
                                    "promotion_desc": bData[0].promotion_desc,
                                    "end_date": bData[0].end_date,
                                    "start_date": bData[0].start_date,
                                    "rating": bData[0].rating,
                                    "bookings": bData[0].bookings,
                                    "views": bData[0].views,
                                    "promoted": bData[0].promoted,
                                    "topic_mode": bData[0].topic_mode,
                                    "image_url": config.liveUrl + config.topicImagePath + bData[0].image_url,
                                    "session_duration": bData[0].session_duration,
                                    "price": bData[0].price,
                                    "learn_description": bData[0].learn_description,
                                    "topic_description": bData[0].topic_description,
                                    "topic_header": bData[0].topic_header,
                                    "sub_cat_name": bData[0].sub_cat_name,
                                    "sub_cat_id": bData[0].sub_cat_id,
                                    "main_cat_name": bData[0].main_cat_name,
                                    "main_cat_id": bData[0].main_cat_id
                                }
                            }
                        })
                    }
                })
            }
        })
    }
    
    s.statics.alreadyReportedData = function(topicData,callback){
        BookingModels.findOne({
            user_id : topicData.user_id,
            topic_id:topicData.topic_id,
            isreported:true
        },function(err,r){
            if(err) console.log(err);
            else{
                callback(r);
            }
        })
    }
    
    s.statics.reportTopic = function (saveData, callback) {
        BookingModels.getTopicData(saveData, function (tD) {
            if (tD === null) {
                saveData.isreported = true;
                new BookingModels(saveData).save(function (err, s) {
                    if(err){
                        callback({
                            "response_code": 5005,
                            "response_message": "INTERNAL DB ERROR",
                            "response_data": {err}
                        })
                    }
                    if(!err){
                        callback({
                            "response_code": 2000,
                            "response_message": "Thanks for your feedback.",
                            "response_data": {}
                        })
                    }
                })
            }
            else {
                
                BookingModels.alreadyReportedData(saveData, function (savedRec) {
                    if(savedRec === null){
                        /******update existing*****/
                        
                            var booking_id = tD._id;
                            
                            BookingModels.update({
                                _id: booking_id
                            }, {
                                $set: {
                                    isreported: true
                                }
                            }, function (err, v) {
                                if (err)
                                    callback({
                                        "response_code": 5005,
                                        "response_message": "INTERNAL DB ERROR",
                                        "response_data": {err}
                                    })
                                if (!err) {
                                    if (v.nModified === 1) {
                                        callback({
                                            "response_code": 2000,
                                            "response_message": "Thanks for your feedback.",
                                            "response_data": {}
                                        })
                                    }
                                }
                            })
                            
                        
                    }else{
                        callback({
                            "response_code": 5000,
                            "response_message": "you have already reported this topic",
                            "response_data": {}
                        });
                    }
                })
                
                
                
            }
        })

    }
    
    s.statics.topicStatusByUser = function(topicData,callback){ //console.log(topicData); return;
        BookingModels.find({
            user_id : topicData.user_id,
            topic_id:topicData.topic_id,
        },function(err,r){
            if(err) console.log(err);
            else{ //console.log(r); return;
                if(r.length > 0){
                    callback({
                        "response_code": 2000,
                        "response_data": r
                    })
                }else{
                    callback({
                        "response_code": 3000,
                        "response_data": {}
                    })
                }
            }
        })
    }
    
    s.statics.getSavedTopicList = function(userData,callback){ //console.log(userData); return;
        
        BookingModels.aggregate([
                {
                    $match: {
                        $and:
                        [
                            {"user_id": userData.user_id},
                            {"issaved": true}
                        ]
                    }
                }, {
                    $sort: {updatedAt: -1}
                },
                {
                    $lookup: {
                        from: "topics",
                        localField: "topic_id",
                        foreignField: "_id",
                        as: "topic_details"
                    }
                },
                {
                    $unwind: "$topic_details"
                }, {
                    $project: {
                        "_id": "$topic_details._id",
                        "updatedAt": "$topic_details.updatedAt",
                        "createdAt": "$topic_details.createdAt",
                        "user_id": "$topic_details.user_id",
                        "rating": "$topic_details.rating",
                        "bookings": "$topic_details.bookings",
                        "views": "$topic_details.views",
                        "promoted": "$topic_details.promoted",
                        "topic_mode":"$topic_details.topic_mode",
                        "image_url": "$topic_details.image_url",
                        "session_duration": "$topic_details.session_duration",
                        "price": "$topic_details.price",
                        "learn_description": "$topic_details.learn_description",
                        "topic_description": "$topic_details.topic_description",
                        "topic_header": "$topic_details.topic_header",
                        "sub_cat_id": "$topic_details.sub_cat_id",
                        "main_cat_id": "$topic_details.main_cat_id",
                        "creator_name":"$topic_details.creator_name"
                    }
                }
            ], function (err, res) { //console.log(res); return;
                if (err) {
                    callback({
                        "response_code": 5005,
                        "response_message": "INTERNAL DB ERROR",
                        "response_data": err
                    })
                }
                if (!err) {
                    if(res.length > 0){
                        callback({ "response_code": 2000, "result": res});
                    }else{
                        callback({ "response_code": 3000, "result": {}});
                    }
                }
            })
        
        //console.log(topicData); return;
        
    }
    
    s.statics.bookingData = function(userId,callback){
        BookingModels.find({
            user_id : userId,
            isbooked:true
        },function(err,r){
            if(err) console.log(err);
            else{
                callback(r);
            }
        })
    }

return s;
}());

module.exports = BookingModels;