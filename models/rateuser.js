var mongoose = require("mongoose");
var bcrypt = require('bcrypt-nodejs');
var mongo = require('mongodb');
var config = require('../config');
var async = require("async");
var ObjectID = mongo.ObjectID;

var RateuserModel = mongoose.model("Rateuser", function(){

	var s = new mongoose.Schema({
		_id:{
			type: String,
			required: true,
		},
		user_id:{
			type: String,
			required: true
		},
                topic_id:{
                    type: String,
			required: true
                },
		rated_user_id:{
			type: String,
			required: true
		},
                rate:{
			type: Number,
			required: true
		},
                review:{
			type: String,
			default: ''
		}
	},
	{
        timestamps: true
    });
    
    s.statics.alreadyRatedData = function(data,callback){
        RateuserModel.findOne({
            user_id : data.user_id,
            topic_id:data.topic_id
        },function(err,r){
            if(err) console.log(err);
            else{
                callback(r);
            }
        })
    }

    s.statics.rateuser = function(data, callback){
        
        RateuserModel.alreadyRatedData(data, function (tD) {
            if (tD === null) {
                data.rated_user_id = data.other_user_id;
                new RateuserModel(data).save(function (err, response_data) {
                    if (!err) {
                        callback({ "response_code":2000,"result":response_data});
                    } else {
                          callback({
                            "response_code":5005,
                            "response_message":"INTERNAL DB ERROR",
                            "response_data":{}
                        })
                    }
                });
            }else{
                callback({
                    "response_code":5000,
                    "response_message":"You have already rated this topic",
                    "response_data":{}
                })
            }
        })
        
    	
    }
    
    s.statics.getAvgRatebyTopic = function(topic_id, callback){
        RateuserModel.aggregate([
            {
                $match: {
                    "topic_id": topic_id
                }
            }, 
            {$group: {_id:null, rate: {$avg:"$rate"} } },
            
        ], function (err, res) {
            if (err) {
                callback({
                    "response_code": 5005,
                    "response_message": "INTERNAL DB ERROR",
                    "response_data": err
                })
            }
            if (!err) {
                if (res.length > 0) {
                    callback({ "response_code": 2000, "result": res});
                } else {
                    callback({ "response_code": 2001, "result": []});
                }
            }
        })
    }
    
    s.statics.reviewList = function(data, callback){
        RateuserModel.aggregate([
        {
            $match: {
                "rated_user_id": data.user_id
            }
        }, {
            $sort: {updatedAt: -1}
        },
        {
            $lookup: {
                from: "users",
                localField: "user_id",
                foreignField: "_id",
                as: "user_details"
            }
        },
        {
            $unwind: "$user_details"
        },
        {
            $project: {
                "user_id": 1,
                "rate": 1,
                "review": 1,
                "fname": "$user_details.fname",
                "lname": "$user_details.lname",
                "image_url":"$user_details.image_url"
            }
        }
        ], function (err, res) { //console.log(res); return;
            if (err) {
                callback({
                    "response_code": 5005,
                    "response_message": "INTERNAL DB ERROR",
                    "response_data": err
                })
            }
            if (!err) {
                callback({ "response_code": 2000, "result": res});
            }
        })
    }

    return s;
}());

module.exports = RateuserModel;