var mongoose = require("mongoose");
var bcrypt = require('bcrypt-nodejs');
var mongo = require('mongodb');
var config = require('../config');
var async = require("async");
var ObjectID = mongo.ObjectID;

var QuestionModel = mongoose.model("Question", function(){

	var s = new mongoose.Schema({
		_id:{
			type: String,
			required: true,
		},
		question:{
			type: String,
			required: true
		}
	},
	{
        timestamps: true
    });
    
    s.statics.saveQuestion = function(data, callback){
        new QuestionModel(data).save(function (err, response_data) {
            if (!err) {
                callback({ "response_code":2000,"result":response_data});
            } else {
                  callback({
                    "response_code":5005,
                    "response_message":"INTERNAL DB ERROR",
                    "response_data":{}
                })
            }
        });
    }
    
    s.statics.getList = function(data, callback){
        QuestionModel.find({
            
        },function(err,r){
            if(err) console.log(err);
            else{ //console.log(r); return;
                if(r.length > 0){
                    callback({
                        "response_code": 2000,
                        "response_data": r
                    })
                }else{
                    callback({
                        "response_code": 3000,
                        "response_data": {}
                    })
                }
            }
        })
    }

    return s;
}());

module.exports = QuestionModel;