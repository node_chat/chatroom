var mongoose = require("mongoose");
var bcrypt = require('bcrypt-nodejs');
var Message = require('./message');
var ConversationModel = mongoose.model("conversation", (function () {

    var s = new mongoose.Schema({
        _id: {
            type: String,
            required: true
        },
        participants: {
            type: Array,
            required: true
        },
        room_name: {
            type: String,
            required: true
        },
        creator_id:{
            type:String,
            required:true
        },
        creator_name:{
            type:String,
            required:true
        },
        enq_id:{
            type:String,
        },
        enq_status:{
            type:String,
        },
        invited_User:{
          type:Array,
          required: true  
        }
    }, {
            timestamps: true
        });

        s.statics.newConversation = (conversationData, callback)=>{
            new ConversationModel(conversationData).save(function(err,res){
                if(err){
                    callback({
                        response_code:5000,
                        error:err
                    });
                }
                if(!err){
                    if(res){
                        var chatData = {
                            content:conversationData.invited_msg,
                            conversationId:res._id,
                            sender:{user_id:conversationData.creator_id,user_name:conversationData.creator_name}
                        }
                        Message.startChat(chatData,function(msgRes){
                            callback({
                                response_code:2000,
                                response_data:res
                            })
                        }) 
                    }
                }
            })
        }
        s.statics.getChatData = (userData,callback)=>{
            ConversationModel.aggregate([
                {
                    $match:{ participants: { $in : [userData.user_id]}}
                },
                {
                    $lookup: {
                        from: "messages",
                        localField: "_id",
                        foreignField: "conversationId",
                        as: "msg"
                    }
                },
                {
                    $unwind:"$msg"
                },{
                   $project : {
                    "sender":"$msg.sender",
                    "receiver":"$msg.receiver",
                    "readStat":"$msg.readStat",
                    "conversationId":"$msg.conversationId",
                    "content":"$msg.content",
                   } 
                }
            ],function(err,c){
                if(err){
                    callback({
                        "response_code": 5002,
                        "response_message": " insufficient information provided",
                        "response_data": {}
                    }) 
                }
                if(!err){
                    callback(c);
                }
            })
        }
    return s;
})());

module.exports = ConversationModel;