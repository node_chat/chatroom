var mongoose = require("mongoose");
var bcrypt = require('bcrypt-nodejs');
var mongo = require('mongodb');
var ObjectID = mongo.ObjectID;
var Message = mongoose.model("Message", (function () {

    var s = new mongoose.Schema({
        _id: {
            type: String,
            required: true
        },
        sender: {
            type:Array,
            required:true
        },
        receiver: {
           type:Array
        },
        content: {
            type: String,
            required: true
        },
        conversationId: {
            type: String,
        },
        readStat: {
            type: Array
        },
        deleteStatus: {
            type: Boolean,
            default: false,
        },
        filesPath:{
            type: String,
            default:''
        }
    }, {
            timestamps: true
        });

    s.statics.startChat = (chatData, callback) => {
        chatData._id = new ObjectID;
        new Message(chatData).save(function (err, res) {
            if (err) {
                console.log(err);
                callback({
                    response_code:5000,
                    error:err
                });
            }
            if (!err) {
                callback(res);
            }
        })
    }
    s.statics.saveNewMsg = (msgData, callback) => {

    }
    return s;
})());
module.exports = Message;