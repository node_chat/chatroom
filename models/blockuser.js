var mongoose = require("mongoose");
var bcrypt = require('bcrypt-nodejs');
var mongo = require('mongodb');
var config = require('../config');
var async = require("async");
var ObjectID = mongo.ObjectID;

var BlockuserModel = mongoose.model("Blockuser", function(){

	var s = new mongoose.Schema({
		_id:{
			type: String,
			required: true,
		},
		user_id:{
			type: String,
			required: true
		},
		blocked_user_id:{
			type: String,
			required: true
		}
	},
	{
        timestamps: true
    });


    s.statics.alreadyBlocked = function(data,callback){
        BlockuserModel.findOne({
            user_id : data.user_id,
            blocked_user_id:data.other_profile_id
        },function(err,r){
            if(err) console.log(err);
            else{
                callback(r);
            }
        })
    }

    s.statics.blockuser = function(data, callback){
        
        BlockuserModel.alreadyBlocked(data, function (tD) {
            if (tD === null) {
                data.user_id = data.user_id;
                data.blocked_user_id = data.other_profile_id;
                new BlockuserModel(data).save(function (err, response_data) {
                    if (!err) {
                        callback({ "response_code":2000,"result":response_data});
                    } else {
                          callback({
                            "response_code":5005,
                            "response_message":"INTERNAL DB ERROR",
                            "response_data":{}
                        })
                    }
                });
            }else{
                callback({
                    "response_code":5000,
                    "response_message":"You have already blocked this user",
                    "response_data":{}
                })
            }
        })
        
    	
    }
    
    s.statics.blockList = function(blockedData, callback){
        BlockuserModel.aggregate([
        {
            $match: {
                "user_id": blockedData.user_id
            }
        }, {
            $sort: {updatedAt: -1}
        },
        {
            $lookup: {
                from: "users",
                localField: "blocked_user_id",
                foreignField: "_id",
                as: "user_details"
            }
        },
        {
            $unwind: "$user_details"
        },
        {
            $project: {
                "_id": 1,
                "fname": "$user_details.fname",
                "lname": "$user_details.lname",
                "social_id": "$user_details.social_id",
                "image_url":"$user_details.image_url"
            }
        }
        ], function (err, res) { //console.log(res); return;
            if (err) {
                callback({
                    "response_code": 5005,
                    "response_message": "INTERNAL DB ERROR",
                    "response_data": err
                })
            }
            if (!err) {
                callback({ "response_code": 2000, "result": res});
            }
        })
    }
    
    s.statics.unblockUser = function (blockedData, callback) {
            BlockuserModel.remove({
                    _id: blockedData.block_id
                }, function(err, v) {
                    if (err){
                        callback({
                            success: false,
                            STATUSCODE: 5005,
                            err: err
                        })
                    }
                    else {
                        callback({
                            STATUSCODE: 2000 //category deleted successfully
                        })
                    }
                });
           
    }

    return s;
}());

module.exports = BlockuserModel;