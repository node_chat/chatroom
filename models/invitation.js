var mongoose = require("mongoose");
var mongo = require('mongodb');
var ObjectID = mongo.ObjectID;
var InvitationModel = mongoose.model("invitation", (function () {

    var s = new mongoose.Schema({
        _id: {
            type: String,
            required: true
        },
        invited_email:{
            type: String,
            required: true
        },
        invited_id:{
            type: String,
            required: true
        },
        invitation_status:{
            type: String,
            enum: ['PENDING', 'ACCEPTED','REFUSED'],
            default: 'PENDING',
            required: true
        },
        room_id:{
            type: String,
            required: true
        },
        invited_by:{
            type:String,
            required:true
        },
        invited_msg:{
            type:String,
        },
    }, {
            timestamps: true
        });

        s.statics.createInvitation = (invitedList,callback)=>{
            new InvitationModel(invitedList).save(function(err,inviRes){
                if(err){
                    callback({
                        response_code:5000,
                        error:err
                    });
                }
                if(!err){
                    callback({
                        response_code:2000,
                        response_data:inviRes
                    })
                }
            })
            
        }
        s.statics.getInvitationList = (user_id,callback)=>{
            InvitationModel.find({
                invited_id:user_id
            },function(err,invitationList){
                if(err){
                    callback({
                        response_code:5000,
                        error:err
                    });  
                }
                if(!err){
                    callback({
                        response_code:2000,
                        response_data:invitationList
                    })
                }
            })
        }
         return s;
})());
module.exports = InvitationModel;