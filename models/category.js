var mongoose = require("mongoose");
var bcrypt = require('bcrypt-nodejs');
var mongo = require('mongodb');
var config = require('../config');
var async = require("async");
var ObjectID = mongo.ObjectID;

var CategoryModel = mongoose.model("Category", function(){

	var s = new mongoose.Schema({
		_id:{
			type: String,
			required: true,
		},
		cat_name:{
			type: String,
			required: true
		},
		category_image:{
			type: String,
			default:''
		},
		sub_category:[
			{
				sub_name:String, 
				_id:String
			}
		]
	},
	{
        timestamps: true
    });

    s.statics.insertCategory = function(data, callback){
    	if(data.sub_category.length>0){
    		data.sub_category.forEach(function (v, i) {
    			data.sub_category[i]._id = new ObjectID; 
    		});
    		console.log(data);
    		//return;
    		new CategoryModel(data).save(function (err, response_data) {
		        if (!err) {
		            console.log("[insert category]");
		            callback({ "response_code":2000,"result":response_data});
		        } else {
		        	console.log(err);
		              /*callback({
		                "response_code":5005,
		                "response_message":"INTERNAL DB ERROR",
		                "response_data":{}
		            })*/
		        }
		    });
    	}
    	else{
			new CategoryModel(data).save(function (err, response_data) {
		        if (!err) {
		            console.log("[insert category]");
		            callback({ "response_code":2000,"result":response_data});
		        } else {
		              callback({
		                "response_code":5005,
		                "response_message":"INTERNAL DB ERROR",
		                "response_data":{}
		            })
		        }
		    });
    	}
    }

    s.statics.categoryList = function(data, callback){
    	CategoryModel.find({},
    		{sub_category:0,updatedAt:0,createdAt:0,__v:0}, 
    		function(err, res){
    			if(err){
    				console.log("error");
    			}
    			if(!err){
    				async.forEach(res, function (item, callback) {
    					item.category_image = item.category_image ? config.liveUrl + config.categoryImagePath + item.category_image : '';
    					//console.log(item.category_image);
    					callback();
                        }, function (err) {
                        	if (err) {
                        		console.log(err);
                        	}
                        	if(!err){
                        		console.log("complete");
                             	callback({ "response_code":2000,"result":res});
                            }
                        });
    				
    			}
    		});
    }
	s.statics.getCatDetails = function (catDetails, callback) {
		if (catDetails.category) {
			console.log(catDetails.category);
			var catArray = catDetails.category.split(',');
			CategoryModel.find({_id: { $in: catArray }},{sub_category:0,updatedAt:0,createdAt:0,__v:0}, function (err, catDetails) {
				if (err) {
					callback({
						"response_code": 5005,
						"response_message": "INTERNAL DB ERROR",
						"response_data": {}
					})
				}
				if (!err) {
					callback(catDetails);
				}
			})

		}
	}
	
	s.statics.getSubCategoryList = function(catId, callback){
		CategoryModel.findOne(
		{ _id: catId },
		{updatedAt:0,createdAt:0,__v:0},
		function(err, res){
			if (err) {
				console.log("error");
			}
			if(!err){
				callback({"response_code":2000, "result": res});
			}
		});
	}

	s.statics.getCatAndSubCatName = function(topicData , callback){
		
	}
        
        s.statics.spotLightCategoryList = function(data, callback){
    	CategoryModel.aggregate([
                {
                    $match: {
                        
                    }
                }, 
                {
                    $sort: {updatedAt: -1}
                },
                { $limit : 3 }
            ], function (err, res) { //console.log(res); return;
                if (err) {
                    callback({
                        "response_code": 5005,
                        "response_message": "INTERNAL DB ERROR",
                        "response_data": err
                    })
                }
                if (!err) {
                    async.forEach(res, function (item, callback) {
                        item.category_image = item.category_image ? config.liveUrl + config.categoryImagePath + item.category_image : '';
                        callback();
                    }, function (err) {
                        if (err) {
                                console.log(err);
                        }
                        if(!err){
                                
                        callback({ "response_code":2000,"result":res});
                        }
                    });
                }
            })
        }
    return s;
}());

module.exports = CategoryModel;